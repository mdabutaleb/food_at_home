@extends('layouts.admin.master')
@section('main_title', 'Admin')
@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $TodayReport->TotalOrder }}">0</span>
                    </div>
                    <div class="desc"> Order so far today</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $TodayReport->TotalItem }}">0</span>
                    </div>
                    <div class="desc"> Total Items</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="{{ url('/reports/7daysorder') }}">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                        <div class="number">
                            @php
                                $value = '';
                                    foreach($SevenDaysOrders as $order){
                                    $value = $value + $order->NoOfOrder;
                                    }
                            @endphp
                            <span data-counter="counterup" data-value="{{ $value }}">0</span>
                        </div>
                        <div class="desc"> Orders in last 7 days</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="{{ url('/reports/7daysitems') }}">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        @php
                            $val = '';
                                foreach($SevenDaysItem as $item){
                                $val = $value + $item->NoOfItem;
                                }
                        @endphp
                        <span data-counter="counterup" data-value="{{ $val }}"></span>
                    </div>
                    <div class="desc"> Total Item in last 7 days</div>
                </div>
            </a>
        </div>
    </div>
@endsection