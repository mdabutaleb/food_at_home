@extends('layouts.admin.master')
@section('style')
    @include('admin.report.style.style')
@endsection
@section('main_title', 'Total Order in last 7 days')
@section('reports', 'active')
@section('7days_order', 'active')
@section('breadcrumb')
    <li>
        <span>Total Order in last 7 days</span>
    </li>
@endsection
@section('content')
    {{--{{ dd($SevenDaysOrders) }}--}}
    <div class="row">
        <div class="col-md-4" id="mytable">
            <table class="table table-stribbed table-border" id="OrderTable">
                <thead>
                <th>Date</th>
                <th>No Of Order</th>
                </thead>
                <tbody>
                @foreach($SevenDaysOrders as $order)
                    <tr>
                        <td>{{ $order->Date }}</td>

                        <td>{{ $order->NoOfOrder }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            <style>
                #chartdiv {
                    width: 100%;
                    height: 500px;
                    font-size: 11px;
                }
            </style>
            <div id="chartdiv"></div>
        </div>
    </div>

    @php
        $newdata = [];
            foreach ($SevenDaysOrders as $key => $order){
               if (isset($order->Date) && isset($order->NoOfOrder)){
                $newdata[$key]['country'] = $order->Date;
                $newdata[$key]['visits'] = $order->NoOfOrder;
               }

            }


                $data = [
                    [
                    "country" => "Bangladesh",
                    "visits" => 50,
                    ],
                    [
                    "country" => "USA",
                    "visits" => 65,
                    ],[
                    "country" => "INDIA",
                    "visits" => 20,
                    ],[
                    "country" => "PAKISTHAN",
                    "visits" => 5,
                    ],[
                    "country" => "JAPAN",
                    "visits" => 25,
                    ],[
                    "country" => "Australia",
                    "visits" => 75,
                    ],[
                    "country" => "Canada",
                    "visits" => 100,
                    ],
                   ];

            $mydata = json_encode($newdata);
    @endphp
    <script>
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "dataProvider": @php echo $mydata @endphp,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    </script>
@endsection