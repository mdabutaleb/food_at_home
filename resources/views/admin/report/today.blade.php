@extends('layouts.admin.master')
@section('style')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection
@section('main_title', 'Today Report')
@section('reports', 'active')
@section('today_report', 'active')
@section('breadcrumb')
    <li>
        <span>Today's Order & Item</span>
    </li>
@endsection
@section('content')
    <div class="col-md-3">

    </div>
    <div class="col-md-6">
        <table class="table table-stribbed table-border">
            <thead>
            <th>Total Order</th>
            <th>Totoal Items</th>
            </thead>
            <tbody>
            <td id="total_order">Loading..</td>
            <td id="total_items">Loading..</td>
            </tbody>
        </table>
    </div>

    <script>
        $.ajax({
            type: "GET",
            url: "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfToday",
            dataType: "json",
            success: function (data) {
                var mydata = data['Data'];
                var TotalOrder = mydata['TotalOrder'];
                var TotalItems = mydata['TotalItem'];
                    $('#total_order').html(TotalOrder);
                    $('#total_items').html(TotalItems);
//                var len = mydata.length;
//                for (var i = 0; i < len; i++) {
//                    var TotalOrder = mydata[i]['TotalOrder'];
//                    var TotalItems = mydata[i]['TotalItem'];
//                    console.log(TotalOrder)
//                    $('#total_order').html(TotalOrder);
//                    $('#total_items').html(TotalItems);
//                }
            },
        });
    </script>

@endsection