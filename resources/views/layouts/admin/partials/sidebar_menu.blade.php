<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200" style="padding-top: 20px">
        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <li class="nav-item start active open">
            <a href="{{ url('/') }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>
        {{--<li class="heading">--}}
        {{--<h3 class="uppercase">Features</h3>--}}
        {{--</li>--}}
        <li class="nav-item @yield('reports')">
            <a href="#" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Reports</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item @yield('today_report') ">
                    <a href="{{ url('/reports/today') }}" class="nav-link">
                        <i class="icon-diamond"></i>
                        <span class="title">Today's Report</span>
                    </a>
                </li>

                <li class="nav-item @yield('7days_order') ">
                    <a href="{{ url('/reports/7daysorder') }}" class="nav-link ">
                        <i class="icon-action-undo"></i>
                        <span class="title">Last 7 Days Orders</span>
                    </a>
                </li>

                <li class="nav-item @yield('7days_items') ">
                    <a href="{{ url('/reports/7daysitems') }}" class="nav-link ">
                        <i class="icon-action-undo"></i>
                        <span class="title">Last 7 Days Items</span>
                    </a>
                </li>

            </ul>
        </li>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Main Menu 2</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="icon-diamond"></i>
                        <span class="title">Sub Menu 1</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="socicon-github"></i>
                        <span class="title"> Sub Menu 2</span>
                    </a>
                </li>

            </ul>
        </li>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Main Menu 3</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="icon-diamond"></i>
                        <span class="title">Sub Menu 1</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="socicon-github"></i>
                        <span class="title"> Sub Menu 2</span>
                    </a>
                </li>

            </ul>
        </li>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Main Menu 4</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="icon-diamond"></i>
                        <span class="title">Sub Menu 1</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="ui_metronic_grid.html" class="nav-link ">
                        <i class="socicon-github"></i>
                        <span class="title"> Sub Menu 2</span>
                    </a>
                </li>

            </ul>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
</div>