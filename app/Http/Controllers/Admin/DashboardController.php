<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $username = 'admin';
        $password = 'admin';
//        $URL='http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/Login';

        $URL_HOME = 'http://dotnet.nerdcastlebd.com/';
        $LOGIN_URL = 'http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/Login';

        $ch = curl_init($URL_HOME);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $home = curl_exec($ch);

        $post = array('username' => $username, 'password' => $password);
        $query = http_build_query($post);

        curl_setopt($ch, CURLOPT_URL, $LOGIN_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $login = json_decode(curl_exec($ch));

        if ($login->ResultState == 'true') {
            session(['user_logged' => true]);
        }

        if (!empty(session('user_logged'))) {
            return redirect('/users');
        }


// further processing ....

//----------------------------
        $todayReportURL = "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfToday";
        $TodayReport = json_decode(file_get_contents($todayReportURL))->Data;


        $OrderURL = "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfSevenDays?type=order";
        $OrderReport = json_decode(file_get_contents($OrderURL));
        $SevenDaysOrder = $OrderReport->Data;

        $ItemUrl = "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfSevenDays?type=item";
        $ItemReport = json_decode(file_get_contents($ItemUrl));
        $SevenDaysItem = $ItemReport->Data;

        $data = [
            'TodayReport' => $TodayReport,
            'SevenDaysOrders' => $SevenDaysOrder,
            'SevenDaysItem' => $SevenDaysItem,
        ];
//        dd($data);

        return view('admin.dashboard', $data);
    }
}
