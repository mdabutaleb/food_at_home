<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getReportByToday()
    {
//        $url = 'http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfSevenDays?type=order'; // path to your JSON file
//        $result = file_get_contents($url, false);
//        dd($result);
        return view('admin.report.today');
    }

    public function getOrderReportOf7Days()
    {
        $url = "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfSevenDays?type=order";
        $OrderReport = json_decode(file_get_contents($url));
        $SevenDaysOrder = $OrderReport->Data;
        $data = [
            'SevenDaysOrders' => $SevenDaysOrder,
        ];
        return view('admin.report.7dyasOrder', $data);
    }

    public function getItemReportOf7Days()
    {
        $ItemUrl = "http://dotnet.nerdcastlebd.com/FoodAtHome/Api/Admin/GetReportOfSevenDays?type=item";
        $ItemReport = json_decode(file_get_contents($ItemUrl));
        $SevenDaysItem = $ItemReport->Data;
        $data = [
            'SevenDaysItem' => $SevenDaysItem,
        ];

        return view('admin.report.7daysitem', $data);
    }
}
